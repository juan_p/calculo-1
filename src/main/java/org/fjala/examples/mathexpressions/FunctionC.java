package org.fjala.examples.mathexpressions;

/**
 *
 * @author Juan Pablo Chavez Arteaga
 */
public class FunctionC implements IFunction{
       
    @Override
    public float f(float x) {
        return (float) ((x * x - 50 * 50) / (x - 50));
    }

    @Override
    public String toString() {
        return "f(x) = (x * x - 50 * 50) / (x - 50)";
    }
}
