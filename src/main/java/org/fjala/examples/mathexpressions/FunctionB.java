package org.fjala.examples.mathexpressions;

/**
 *
 * @author Juan Pablo Chavez Arteaga
 */
public class FunctionB implements IFunction {

    @Override
    public float f(float x) {
        return 100 * Math.abs(x) / x;
    }

    @Override
    public String toString() {
        return "f(x) = 100 * Math.abs(x) / x";
    }
    
}
