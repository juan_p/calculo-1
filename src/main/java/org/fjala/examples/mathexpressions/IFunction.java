package org.fjala.examples.mathexpressions;

public interface IFunction {
    float f(float x);
}
