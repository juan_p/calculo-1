package org.fjala.examples.mathexpressions;

import java.util.Random;
/**
 *
 * @author Juan Pablo Chavez Arteaga
 */
public class Calculo1 {
    
    public static void main(String[] args) {
        double zero = 0;
        double variable = Math.round(new Random().nextDouble() * 100);
        
        System.out.println("x = " + variable);
        System.out.println("x/0 = " + variable / zero);
        System.out.println("1) 0/0 = " + zero / zero);
        System.out.println("2) Division de infinitos = " + (Double.POSITIVE_INFINITY / Double.POSITIVE_INFINITY));
        System.out.println("3) 0 * Infinity = " + zero * Double.POSITIVE_INFINITY);
        System.out.println("4) 1 elevado al infinito = " + Math.pow(1, Double.POSITIVE_INFINITY));
        System.out.println("5) 0 elevado a 0 = " + Math.pow(0, 0));
        System.out.println("6) infinito elevado a 0 = " + Math.pow(Double.POSITIVE_INFINITY, 0));
        System.out.println("7) \"anulacion\" de infinitos = " + (Double.POSITIVE_INFINITY + Double.NEGATIVE_INFINITY));
        
    }
}
