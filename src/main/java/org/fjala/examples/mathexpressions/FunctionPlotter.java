package org.fjala.examples.mathexpressions;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FunctionPlotter {

    private int imageWidth;
    private int imageHeight;
    private BufferedImage image;
    private Graphics2D g;

    public FunctionPlotter(int imageWidth, int imageHeight) {
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        this.g = this.image.createGraphics();
    }

    public void prepare() {
        this.g.setColor(Color.white);
        this.g.fillRect(0, 0, this.imageWidth, this.imageHeight);

        this.g.setColor(Color.GRAY);
        for (int i = 0; i < imageHeight; i += 50) {
            this.g.drawLine(0, i, imageWidth, i);
        }
        for (int i = 0; i < imageWidth; i += 50) {
            this.g.drawLine(i, 0, i, imageHeight);
        }

        this.g.setColor(Color.RED);
        this.g.drawLine(imageWidth / 2, 0, imageWidth / 2, imageHeight);
        this.g.drawLine(0, imageHeight / 2, imageWidth, imageHeight / 2);

        this.g.setColor(Color.BLACK);
        this.g.drawString("X", imageWidth - 10, (imageHeight / 2) - 10);
        this.g.drawString("Y", (imageWidth / 2) - 10, 10);
        for (int posX = -(imageWidth / 2); posX <= imageWidth / 2; posX += 50) {
            if (posX != 0) {
                this.g.drawString("" + posX, posX + (imageWidth / 2) + 2, (imageHeight / 2));
            }
        }
        for (int posY = imageHeight / 2; posY >= -imageHeight / 2; posY -= 50) {
            this.g.drawString("" + posY, (imageWidth / 2), -posY + (imageHeight / 2));
        }
        /* Examples
        this.g.setColor(Color.red);
        this.g.drawLine(100,100,400,100);

        this.g.setColor(Color.gray);
        this.g.drawString("Hello World!", 0, 50);
         */
    }

    public void plot(IFunction function) {
        float domainFrom = this.imageWidth / 2;
        float domainTo = -this.imageWidth / 2;

        this.g.setColor(Color.BLUE);
        this.g.drawString(function.toString(), 0, 25);

        int lastU = 0;
        int lastV = 0;

        boolean firstPoint = true;

        this.g.setColor(Color.blue);

        for (float x = domainFrom; x >= domainTo; x -= 1) {
            float y = function.f(x);
            if (Float.isFinite(y)) {
                // You need to take a look to your homework
                int u = (int) x + imageWidth / 2;
                int v = (int) -y + imageHeight / 2;

                if (!firstPoint) {
                    this.g.drawLine(lastU, lastV, u, v);
                } else {
                    firstPoint = false;
                }

                lastU = u;
                lastV = v;
            } else {
                firstPoint = true;
                this.g.drawOval(lastU - 4, lastV -  3, 6, 6);
            }
        }
    }

    public void save(String pngPath) throws IOException {
        File file = new File(pngPath);
        ImageIO.write(this.image, "png", file);
    }
}
