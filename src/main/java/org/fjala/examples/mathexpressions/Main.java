package org.fjala.examples.mathexpressions;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        int imageWidth = 500;
        int imageHeight = 500;
        String pngPath = "my-function.png";

        FunctionPlotter plotter = new FunctionPlotter(imageWidth, imageHeight);
        plotter.prepare();
        plotter.plot(new FunctionA());
        plotter.save("Function-A.png");
        
        plotter.prepare();
        plotter.plot(new FunctionB());
        plotter.save("Function-B.png");
        
        plotter.prepare();
        plotter.plot(new FunctionC());
        plotter.save("Function-C.png");
        
        plotter.prepare();
        plotter.plot(new FunctionD());
        plotter.save("Function-D.png");

        System.out.println("The image was generated at location: " + pngPath);
    }
}
