package org.fjala.examples.mathexpressions;

/**
 *
 * @author Juan Pablo Chavez Arteaga
 */
public class FunctionD implements IFunction {

    @Override
    public float f(float x) {
        return (float) (2000 * Math.sin(x / 10) / x);
    }

    @Override
    public String toString() {
        return "f(x) = (x * x - 50 * 50) / (x - 50)";
    }
    
}
