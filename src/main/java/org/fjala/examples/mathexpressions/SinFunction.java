package org.fjala.examples.mathexpressions;

/**
 *
 * @author Juan Pablo Chavez Arteaga
 */
public class SinFunction implements IFunction {
    
    @Override
    public float f(float x){
        return (float) (100 * Math.sin(x * Math.PI / 100) );
    }
    
    @Override
    public String toString(){
        return "f(x) = 100 * sin(x * pi * 100)";
    }
}
