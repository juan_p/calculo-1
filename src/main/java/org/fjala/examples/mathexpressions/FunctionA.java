package org.fjala.examples.mathexpressions;

/**
 *
 * @author Juan Pablo Chavez Arteaga
 */
public class FunctionA implements IFunction{

    @Override
    public float f(float x){
        return (float) (x * Math.sqrt(10) / (Math.sqrt(x + 10) - Math.sqrt(10)));
    }
    
    @Override
    public String toString(){
        return "f(x) = x * Math.sqrt(10) / (Math.sqrt(x + 10) - Math.sqrt(10))";
    }
    
}
