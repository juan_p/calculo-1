package org.fjala.examples.mathexpressions;

public class CuadraticFunction implements IFunction {
    public float f(float x) {
        return (x * x) / 50;// dividido entre 50 por visibilidad
    }
    
    @Override
    public String toString(){
        return "f(x) = x * x";
    }
}
