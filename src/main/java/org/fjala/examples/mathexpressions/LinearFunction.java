package org.fjala.examples.mathexpressions;

public class LinearFunction implements IFunction {
    public float f(float x) {
        // f(x) = x
        return x;
    }
    
    @Override
    public String toString(){
        return "f(x) = x";
    }
}
